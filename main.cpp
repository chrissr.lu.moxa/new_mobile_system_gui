#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <checkconnect.h>
#include <callprocess.h>

int main(int argc, char *argv[])
{

    qmlRegisterType<CheckConnect>("checkconnect", 1, 0, "CheckConnect");
    qmlRegisterType<CallProcess>("callprocess", 1, 0, "CallProcess");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
