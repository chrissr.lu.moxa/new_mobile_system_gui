#ifndef CHECKCONNECTION_H
#define CHECKCONNECTION_H

#include <QObject>
#include <QProcess>
#include <QString>
#include <QThread>

class CheckConnect : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int is_connect_ros READ get_ros_status WRITE set_ros_status NOTIFY change_ros_status)
    Q_PROPERTY(int is_connect_face READ get_face_status WRITE set_face_status NOTIFY change_face_status)
public:
    explicit CheckConnect(QObject *parent = nullptr);
    Q_INVOKABLE void check_all_status();


    int get_ros_status() {return this->is_connect_ros;}
    int get_face_status() {return this->is_connect_face;}

    void set_ros_status(const int status) {
        this->is_connect_ros = status;
        emit change_ros_status(status);
    }
    void set_face_status(const int status) {
        this->is_connect_face = status;
        emit change_face_status(status);
    }

signals:
    void change_ros_status(int status);
    void change_face_status(int status);

public slots:

private:
    int is_connect_ros = -1;
    int is_connect_face = -1;
    QString ros_ip = "140.112.14.208";
    QString face_ip = "192.168.31.4";
};

#endif // CHECKCONNECTION_H
