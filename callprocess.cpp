#include "callprocess.h"

CallProcess::CallProcess(QObject *parent) : QObject(parent)
{

}


void CallProcess::call_robotv5()
{
    QProcess *process = new QProcess(this);
    process->start(ROBOT_V5);
    process->startDetached();
}


void CallProcess::call_slammot_constructor()
{
    QProcess *process = new QProcess(this);
    process->start(SLAMMOT_CONS_EXE);
    process->startDetached();
}

void CallProcess::call_slammot_navigation()
{

}

void CallProcess::call_head()
{

}

void CallProcess::call_face()
{
    QProcess *process = new QProcess(this);
    process->start("ncat", QStringList() << AGX_IP << FACE_PORT);
    process->startDetached();
}

void CallProcess::call_agx()
{
    QProcess *process = new QProcess(this);
    process->start("ncat", QStringList() << AGX_IP << AGX_PORT);
    process->startDetached();
}
