#include "checkconnect.h"

CheckConnect::CheckConnect(QObject *parent) : QObject(parent)
{

}

void CheckConnect::check_all_status() {
    // check ros
    int exitCode;
    QProcess *ros_p = new QProcess(this);
    QProcess *face_p = new QProcess(this);
#if defined(_WIN32) || defined(_WIN64)
    exitCode = ros_p->execute("ping", QStringList() << "-w" << "30" << "-n" << "1" << this->ros_ip);
    if(exitCode == 0) {
        this->set_ros_status(0);
    }
    exitCode = face_p->execute("ping", QStringList() << "-w" << "30" << "-n" << "1" << this->face_ip);
    if(exitCode == 0) {
        this->set_face_status(0);
    }
#elif __linux__
    ros_p->execute("ping", QStringList() << "-c" << "1" << this->ros_ip);
#endif
    // excute
    /*
     *
     *
     * https://forum.qt.io/topic/95228/run-process-in-background-without-blocking/10
     */

}
