import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3

import checkconnect 1.0
import callprocess 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 1280
    height: 720
    color: "#2e2f30"
    title: qsTr("New Mobile System")

    Item {
        id: slam_wrapper
        x: 61
        y: 63
        width: 663
        height: 297

        Rectangle {
            color: "#00000000"
            radius: 3
            anchors.fill: parent
            border.color: "#9e9e9e"
            border.width: 2
        }


        Item {
            id: slammot
            x: 40
            width: 234
            height: 179
            anchors.top: system_selector.bottom
            anchors.topMargin: 0

            StatusIndicator {
                id: slammotIndicator
                x: 20
                y: 26
                active: true
            }

            Button {
                id: mot_construct_bt
                x: 77
                y: 69
                width: 140
                height: 48
                text: qsTr("Construct Map")
                highlighted: true
                onClicked: {
                    process.call_slammot_constructor();
                }
            }

            Button {
                id: mot_nav_bt
                x: 77
                y: 123
                width: 140
                height: 48
                text: qsTr("Navigation")
                highlighted: true

            }

            Text {
                id: mot_status
                x: 77
                y: 21
                width: 149
                height: 34
                color: "#ffffff"
                text: qsTr("Running....")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 15
                font.bold: true
                font.family: "Open Sans"
            }


        }

        RowLayout {
            id: system_selector
            x: 165
            width: 564
            height: 46
            spacing: 0
            anchors.top: parent.top
            anchors.topMargin: 50
            anchors.horizontalCenter: parent.horizontalCenter

            RadioDelegate {
                id: slammotDelegate
                text: qsTr("SLAMMOT")
                hoverEnabled: false
                display: AbstractButton.IconOnly

                Text {
                    id: element2
                    y: 6
                    color: "#ffffff"
                    text: qsTr("SLAMMOT")
                    anchors.left: parent.right
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                    font.family: "Open Sans"
                    font.pixelSize: 15
                }
            }

            RadioDelegate {
                id: rosDelegate
                text: qsTr("ROS SLAM")
                hoverEnabled: false
                display: AbstractButton.IconOnly

                Text {
                    id: element3
                    y: 5
                    color: "#ffffff"
                    text: qsTr("ROS")
                    anchors.left: parent.right
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                    font.family: "Open Sans"
                    font.pixelSize: 16
                }
            }


        }

        Text {
            id: slam
            color: "#ffffff"
            text: qsTr("SLAM")
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            font.bold: true
            font.pixelSize: 22
            font.family: "Open Sans"
        }

        Item {
            id: ros
            x: 328
            width: 234
            height: 179
            anchors.topMargin: 0
            StatusIndicator {
                id: rosIndicator1
                x: 20
                y: 26
                active: true
            }

            Button {
                id: ros_construct_bt
                x: 77
                y: 69
                width: 140
                height: 48
                text: qsTr("Construct Map")
                autoRepeat: false
                flat: false
                highlighted: true
            }

            Button {
                id: ros_nav_bt
                x: 77
                y: 123
                width: 140
                height: 48
                text: qsTr("Navigation")
                highlighted: true
            }

            Text {
                id: ros_status
                x: 77
                y: 21
                width: 149
                height: 34
                color: "#ffffff"
                text: qsTr("Running....")
                font.pixelSize: 15
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                horizontalAlignment: Text.AlignLeft
                font.family: "Open Sans"
            }
            anchors.top: system_selector.bottom
        }


    }

    Item {
        id: arm_wrapper
        x: 61
        y: 430
        width: 663
        height: 213
        Rectangle {
            color: "#00000000"
            radius: 3
            border.color: "#9e9e9e"
            anchors.fill: parent
            border.width: 2
        }

        Text {
            id: hand
            color: "#ffffff"
            text: qsTr("Hand & ARM")
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.left: parent.left
            font.pixelSize: 22
            anchors.top: parent.top
            font.bold: true
            font.family: "Open Sans"
        }

        RowLayout {
            id: rowLayout1
            x: 88
            y: 88
            width: 488
            height: 100
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0
            transformOrigin: Item.Center

            Button {
                id: left_hand_bt
                text: qsTr("DUAL ARM")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                highlighted: true
                Material.accent: Material.Teal
            }

            Button {
                id: right_hand_bt
                text: qsTr("DUAL HAND")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                highlighted: true
                Material.accent: Material.Teal
            }

            Button {
                id: dual_bt
                text: qsTr("Dual Hand Arm")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                highlighted: true
                Material.accent: Material.Teal
                onClicked: {
                    process.call_robotv5();
                }
            }
        }
    }

    Item {
        id: expression_wrapper
        x: 825
        y: 63
        width: 393
        height: 246
        Rectangle {
            color: "#00000000"
            radius: 3
            anchors.bottomMargin: 0
            border.color: "#9e9e9e"
            anchors.fill: parent
            border.width: 2
        }

        Text {
            id: expression
            color: "#ffffff"
            text: qsTr("AGX & Expression")
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.left: parent.left
            font.pixelSize: 22
            anchors.top: parent.top
            font.bold: true
            font.family: "Open Sans"
        }

        Item {
            id: expression_all
            x: 81
            y: 72
            width: 231
            height: 137

            Button {
                id: active_expression_bt
                x: 23
                y: 26
                width: 140
                height: 48
                text: qsTr("Face")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                highlighted: true
                Material.accent: Material.Indigo
                onClicked: {
                    process.call_face();
                }
            }

            StatusIndicator {
                id: expressIndicator
                active: true
            }

            Text {
                id: express_status
                x: 43
                y: -5
                width: 149
                height: 34
                color: "#ffffff"
                text: qsTr("Running....")
                font.pixelSize: 15
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                horizontalAlignment: Text.AlignLeft
                font.family: "Open Sans"
            }

            Button {
                id: active_expression_bt1
                x: 21
                y: 31
                width: 140
                height: 48
                text: qsTr("AGX System")
                anchors.verticalCenterOffset: 54
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                highlighted: true
                Material.accent: Material.Indigo
                anchors.verticalCenter: parent.verticalCenter
                onClicked: {
                    process.call_agx();
                }
            }
        }
    }

    Item {
        id: head_wrapper
        x: 825
        y: 357
        width: 393
        height: 128
        Rectangle {
            color: "#00000000"
            radius: 3
            border.color: "#9e9e9e"
            anchors.fill: parent
            border.width: 2
        }

        Text {
            id: head
            color: "#ffffff"
            text: qsTr("Head")
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.left: parent.left
            font.pixelSize: 22
            anchors.top: parent.top
            font.bold: true
            font.family: "Open Sans"
        }

        Button {
            id: head_bt
            x: 127
            y: 82
            width: 140
            height: 48
            text: qsTr("Control Panel")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            highlighted: true
            Material.accent: Material.Red
        }
    }

    CheckConnect {
        id: check_connect
        Component.onCompleted: {
            check_all_status()
            if(is_connect_face && is_connect_ros) timer.running = false;
            if (check_connect.is_connect_ros == 0) {
                rosIndicator1.color = "green";
            } else {
                rosIndicator1.color = "red";
            }
            if (check_connect.is_connect_face == 0) {
                expressIndicator.color = "green"
            } else {
                expressIndicator.color = "red";
            }
        }
    }

    CallProcess {
        id: process
    }


    Timer {
        id: timer
        interval: 10000;
        running: true;
        repeat: true;
        onTriggered: {
            if (check_connect.is_connect_ros != 0 || check_connect.is_connect_face) {
                check_connect.check_all_status();
                if (check_connect.is_connect_ros == 0) {
                    rosIndicator1.color = "green";
                } else {
                    rosIndicator1.color = "red";
                }
                if (check_connect.is_connect_face == 0) {
                    expressIndicator.color = "green"
                } else {
                    expressIndicator.color = "red";
                }
            } else {
                timer.running = false;
            }

        }
    }

}




