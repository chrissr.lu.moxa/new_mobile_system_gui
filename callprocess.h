#ifndef CALLPROCESS_H
#define CALLPROCESS_H

#include <QObject>
#include <QProcess>

#define SLAMMOT_CONS_EXE "C:/Users/RoboticsLab/Desktop/Construct_SLAMMOT/release/RobotDemo.exe"
#define ROBOT_V5 "C:/Users/RoboticsLab/Desktop/dual_arm_mobile/new_robot_impdedance/S03-ADSServer/Release/Robot_v4.exe"

#define AGX_IP "192.168.31.4"
#define FACE_PORT "3000"
#define AGX_PORT "3001"

class CallProcess : public QObject
{
    Q_OBJECT
public:
    Q_INVOKABLE void call_robotv5();
    Q_INVOKABLE void call_head();
    Q_INVOKABLE void call_slammot_constructor();
    Q_INVOKABLE void call_slammot_navigation();
    Q_INVOKABLE void call_face();
    Q_INVOKABLE void call_agx();
    explicit CallProcess(QObject *parent = nullptr);

signals:




};

#endif // CALLPROCESS_H
